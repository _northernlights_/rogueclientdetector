## Rogue Client Detector ##

Detects potential rogue clients on the network by checking Windows server (2003 and above) DHCP leases. Reports by email and logs to the event viewer.

### How does it work? ###

It's a service and a small GUI to configure and test it. The service checks DHCP leases, reports to the event viewer and sends emails. All requests to the DHCP server are made using native Win32 API calls. The GUI sits in the notification area. Click the ninja icon to show options.

### Installation ###

To install, run the .msi. You can install it on any machine that’s in the domain, not necessarily the DHCP server. During install you will be asked for a user name and password that the service will log on as; that user needs to be admin of the target DHCP server.

### Configuration ###

To configure, in the notification area menu, click "Config". From there, indicate the DHCP server to check, how often to check it, whether or not to send email notifications, configure SMTP settings, and the rogue filter.

![screenshot00139.png](https://bitbucket.org/repo/kj6L87/images/2463378195-screenshot00139.png)
![screenshot00140.png](https://bitbucket.org/repo/kj6L87/images/3614593645-screenshot00140.png)

![screenshot00141.png](https://bitbucket.org/repo/kj6L87/images/502423516-screenshot00141.png)

The rogue filter is a regular expression. Host names found in DHCP leases that DON'T match the regular expression are considered rogues and reported.

### Test ###

You can monitor and test it using “test”

![screenshot00143.png](https://bitbucket.org/repo/kj6L87/images/3666973780-screenshot00143.png)

![screenshot00144.png](https://bitbucket.org/repo/kj6L87/images/1307992435-screenshot00144.png)

### License ###

Rogue Client Detector is released under the terms of the [GPL v3](http://www.gnu.org/licenses/gpl-3.0.en.html).