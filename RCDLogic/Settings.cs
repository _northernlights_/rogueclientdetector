﻿/*
 *  Rogue Client Detector - Copyright 2015 Clement Lorteau
 * 
 *  This file is part of Rogue Client Detector.
 *
 *  Rogue Client Detector is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Rogue Client Detector is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rogue Client Detector.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RCDLogic
{
    public class Settings : INotifyPropertyChanged
    {
        private uint _checkInterval;
        private string _dhcpServer;
        private bool _sendEmail;
        private string _smtpServer;
        private uint _smtpPort;
        private string _fromAddress;
        private string _toAddress;
        private string _subject;
        private string _detectionRegEx;
        private static string _fileName
        {
            get
            {
                string folder = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
                string subfolder = Path.Combine(folder, "RogueClientDetector");
                if(!Directory.Exists(subfolder))
                {
                    Directory.CreateDirectory(subfolder);
                }
                return Path.Combine(subfolder, "settings.xml");
            }
        }

        public uint CheckInterval
        {
            get { return _checkInterval; }
            set
            {
                if (_checkInterval == value) return;
                _checkInterval = value;
                NotifyPropertyChanged("CheckInterval");
            }
        }

        public string DHCPServer
        {
            get { return _dhcpServer; }
            set
            {
                if (_dhcpServer == value) return;
                _dhcpServer = value;
                NotifyPropertyChanged("DHCPServer");
            }
        }

        public bool SendEmail
        {
            get { return _sendEmail; }
            set
            {
                if (_sendEmail == value) return;
                _sendEmail = value;
                NotifyPropertyChanged("SendEmail");
            }
        }

        public string SMTPServer
        {
            get { return _smtpServer; }
            set
            {
                if (value == _smtpServer) return;
                _smtpServer = value;
                NotifyPropertyChanged("SMTPServer");
            }
        }

        public uint SMTPPort
        {
            get { return _smtpPort; }
            set
            {
                if (value == _smtpPort) return;
                _smtpPort = value;
                NotifyPropertyChanged("SMTPPort");
            }
        }

        public string FromAddress
        {
            get { return _fromAddress; }
            set
            {
                if (value == _fromAddress) return;
                _fromAddress = value;
                NotifyPropertyChanged("FromAddress");
            }
        }

        public string ToAddress
        {
            get { return _toAddress; }
            set
            {
                if (value == _toAddress) return;
                _toAddress = value;
                NotifyPropertyChanged("ToAddress");
            }
        }

        public string Subject
        {
            get { return _subject; }
            set
            {
                if (value == _subject) return;
                _subject = value;
                NotifyPropertyChanged("Subject");
            }
        }

        public string DetectionRegEx
        {
            get { return _detectionRegEx; }
            set
            {
                if (value == _detectionRegEx) return;
                _detectionRegEx = value;
                NotifyPropertyChanged("DetectionRegEx");
            }
        }

        public Settings()
        {
            //defaults
            CheckInterval = 5;
            DHCPServer = "127.0.0.1";
            SendEmail = true;
            SMTPServer = "127.0.0.1";
            SMTPPort = 25;
            Subject = "Rogue client(s) detected!";
            DetectionRegEx = @"[A-Za-z]{3,4}\d{0,1}-\d{3,4}";
        }

        public static Settings Load()
        {
            Settings result;
            try
            {
                using (StreamReader srReader = new StreamReader(_fileName))
                {
                    XmlSerializer deserializer = new XmlSerializer(typeof(Settings));
                    result = (Settings)deserializer.Deserialize(srReader);
                }
 
            }
            catch (Exception oops)
            {
                Debug.WriteLine("Could not load settings: " + oops.Message);
                result = new Settings();
            }
            return result;
        }

        public void Save()
        {
            using (StreamWriter srWriter = new StreamWriter(_fileName))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                serializer.Serialize(srWriter, this);
            }

            Debug.WriteLine("Settings saved");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Validate()
        {
            if (String.IsNullOrEmpty(DHCPServer)) throw new Exception("DHCP server IP/host name is not configured");
            if (SendEmail)
            {
                if (String.IsNullOrEmpty(SMTPServer)) throw new Exception("SMTP server IP/host name is not configured but email notifications are enabled");
                if (String.IsNullOrEmpty(ToAddress)) throw new Exception("Email TO address is not configured but email notifications are enabled");
                if (String.IsNullOrEmpty(FromAddress)) throw new Exception("Email FROM address is not configured but email notifications are enabled");
            }
        }
    }
}
