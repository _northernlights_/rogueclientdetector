﻿/*
 *  Rogue Client Detector - Copyright 2015 Clement Lorteau
 * 
 *  This file is part of Rogue Client Detector.
 *
 *  Rogue Client Detector is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Rogue Client Detector is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rogue Client Detector.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RCDLogic.DHCP
{
    public static class DHCPSubnetsFinder
    {
        public static List<DHCPSubnet> FindAllDHCPSubnets(string server)
        {
            List<DHCPSubnet> foundSubnets = new List<DHCPSubnet>();

            uint resumeHandle = 0;
            uint numSubnetsRead = 0;
            uint totalSubnets = 0;

            IntPtr info_array_ptr;  

            uint response = DHCPSAPIDLL.DhcpEnumSubnets(
                server,
                ref resumeHandle,
                65536,
                out info_array_ptr,
                ref numSubnetsRead,
                ref totalSubnets
                );

            if (response == 0)
            {
                DHCP_IP_ARRAY rawSubnets =
                    (DHCP_IP_ARRAY)Marshal.PtrToStructure(info_array_ptr, typeof(DHCP_IP_ARRAY));

                int size = (int)rawSubnets.NumElements;
                IntPtr outArray = rawSubnets.Elements;
                DHCP_IP_ADDRESS[] ipAddressArray = new DHCP_IP_ADDRESS[size];
                IntPtr current = outArray;

                for (int i = 0; i < size; i++ )
                {
                    ipAddressArray[i] = new DHCP_IP_ADDRESS();
                    Marshal.PtrToStructure(current, ipAddressArray[i]);
                    Marshal.DestroyStructure(current, typeof(DHCP_IP_ADDRESS));

                    //move to next subnet
                    current = (IntPtr)((int)current + Marshal.SizeOf(ipAddressArray[i]));

                    foundSubnets.Add(new DHCPSubnet { ip = DHCPSAPIDLL.UInt32IPAddressToString(ipAddressArray[i].IPAddress) });
                }
                Marshal.FreeCoTaskMem(outArray);
            }
            else
            {
                int code = 0;
                unchecked
                {
                    code = (int)response;
                }
                Win32Exception winex = new Win32Exception(code);
                throw winex;
            } 

            return foundSubnets;
        }
    }
}
