﻿/*
 *  Rogue Client Detector - Copyright 2015 Clement Lorteau
 * 
 *  This file is part of Rogue Client Detector.
 *
 *  Rogue Client Detector is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Rogue Client Detector is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rogue Client Detector.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RCDLogic.DHCP
{
    public static class DHCPClientsStorage
    {
        private static TimeSpan _retentionPeriod = TimeSpan.FromDays(1);
        private static string _fileName
        {
            get
            {
                string folder = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
                string subfolder = Path.Combine(folder, "RogueClientDetector");
                if(!Directory.Exists(subfolder))
                {
                    Directory.CreateDirectory(subfolder);
                }
                return Path.Combine(subfolder, "dhcpclients.xml");
            }
        }

        //update list with only newly detected clients
        public static List<DHCPClient> Update(List<DHCPClient> detectedclients)
        {
            List<DHCPClient> previousclients = Load();

            //clean up old clients
            previousclients.RemoveAll(x => DateTime.Now - x.detectionTime > _retentionPeriod);

            //keep only new clients
            List<DHCPClient> newclients = new List<DHCPClient>();
            foreach (DHCPClient client in detectedclients)
            {
                //find out if previous clients contain this client and keep that client if not
                bool duplicate = false;
                foreach (DHCPClient previousclient in previousclients)
                {
                    if (previousclient.ip == client.ip)
                    {
                        duplicate = true;
                        break;
                    }                    
                }
                if (!duplicate) newclients.Add(client);
            }

            //save the previous non-expired clients + the new clients
            List<DHCPClient> clientstosave = new List<DHCPClient>();
            foreach (DHCPClient c in newclients) clientstosave.Add(c);
            foreach (DHCPClient c in previousclients) clientstosave.Add(c);
            Save(clientstosave);

            //return the list of newly detected clients
            return newclients;
        }

        private static void Save(List<DHCPClient> newclients)
        {
            using (StreamWriter srWriter = new StreamWriter(_fileName))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<DHCPClient>));
                serializer.Serialize(srWriter, newclients);
            }

            Debug.WriteLine("Clients list saved");
        }

        private static List<DHCPClient> Load()
        {
            List<DHCPClient> result;
            try
            {
                using (StreamReader srReader = new StreamReader(_fileName))
                {
                    XmlSerializer deserializer = new XmlSerializer(typeof(List<DHCPClient>));
                    result = (List<DHCPClient>)deserializer.Deserialize(srReader);
                }

            }
            catch (Exception oops)
            {
                Debug.WriteLine("Could not load DHCP clients list: " + oops.Message);
                result = new List<DHCPClient>();
            }

            return result;
        }
    }
}
