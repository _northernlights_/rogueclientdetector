﻿/*
 *  Rogue Client Detector - Copyright 2015 Clement Lorteau
 * 
 *  This file is part of Rogue Client Detector.
 *
 *  Rogue Client Detector is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Rogue Client Detector is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rogue Client Detector.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RCDLogic.DHCP
{
    // http://www.ianatkinson.net/computing/dhcpcsharp.htm
    public static class DHCPClientsFinder
    {
        public static List<DHCPClient> FindAllDHCPClients(string server, string subnet)  
        {  
            List<DHCPClient> foundClients = new List<DHCPClient>(); 
  
            uint parsedMask     = DHCPSAPIDLL.StringIPAddressToUInt32(subnet);  
            uint resumeHandle   = 0;  
            uint numClientsRead = 0;  
            uint totalClients   = 0;  
  
            IntPtr info_array_ptr;  
  
            uint response = DHCPSAPIDLL.DhcpEnumSubnetClients(  
                server,  
                parsedMask,  
                ref resumeHandle,  
                65536,  
                out info_array_ptr,  
                ref numClientsRead,  
                ref totalClients  
                );

            if (response == 0)
            {

                // set up client array casted to a DHCP_CLIENT_INFO_ARRAY  
                // using the pointer from the response object above  

                DHCP_CLIENT_INFO_ARRAY rawClients =
                    (DHCP_CLIENT_INFO_ARRAY)Marshal.PtrToStructure(info_array_ptr, typeof(DHCP_CLIENT_INFO_ARRAY));

                // loop through the clients structure inside rawClients   
                // adding to the dchpClient collection  

                IntPtr current = rawClients.Clients;

                for (int i = 0; i < (int)rawClients.NumElements; i++)
                {
                    // 1. Create machine object using the struct  

                    DHCP_CLIENT_INFO rawMachine =
                        (DHCP_CLIENT_INFO)Marshal.PtrToStructure(Marshal.ReadIntPtr(current), typeof(DHCP_CLIENT_INFO));

                    // 2. create new C# dhcpClient object and add to the   
                    // collection (for hassle-free use elsewhere!!)  

                    DHCPClient thisClient = new DHCPClient();

                    thisClient.ip = DHCPSAPIDLL.UInt32IPAddressToString(rawMachine.ip);

                    thisClient.hostname = rawMachine.ClientName;

                    thisClient.mac = String.Format("{0:x2}{1:x2}.{2:x2}{3:x2}.{4:x2}{5:x2}",
                        Marshal.ReadByte(rawMachine.mac.Data),
                        Marshal.ReadByte(rawMachine.mac.Data, 1),
                        Marshal.ReadByte(rawMachine.mac.Data, 2),
                        Marshal.ReadByte(rawMachine.mac.Data, 3),
                        Marshal.ReadByte(rawMachine.mac.Data, 4),
                        Marshal.ReadByte(rawMachine.mac.Data, 5));

                    thisClient.detectionTime = DateTime.Now;

                    foundClients.Add(thisClient);

                    // 3. move pointer to next machine  

                    current = (IntPtr)((int)current + (int)Marshal.SizeOf(typeof(IntPtr)));
                }
            }

            else
            {
                int code = 0;
                unchecked
                {
                    code = (int)response;
                }
                Win32Exception winex = new Win32Exception(code);
                throw winex;
            } 
  
            return foundClients;  
        }  
     
    }

}
