﻿/*
 *  Rogue Client Detector - Copyright 2015 Clement Lorteau
 * 
 *  This file is part of Rogue Client Detector.
 *
 *  Rogue Client Detector is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Rogue Client Detector is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rogue Client Detector.  If not, see <http://www.gnu.org/licenses/>.
 */

using RCDLogic.DHCP;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RCDLogic
{
    public static class Detection
    {
        public static List<DHCPClient> Run()
        {
            Settings settings = Settings.Load();
            settings.Validate(); //will throw exception if something is wrong

            //find all dhcp clients on target dhcp server
            List<DHCPClient> allClients = new List<DHCPClient>();
            foreach (DHCPSubnet dhcpsubnet in DHCPSubnetsFinder.FindAllDHCPSubnets(settings.DHCPServer))
            {
                foreach (DHCPClient dhcpclient in DHCPClientsFinder.FindAllDHCPClients(settings.DHCPServer, dhcpsubnet.ip))
                {
                    allClients.Add(dhcpclient);
                }
            }

            //filter out non rogue ones
            List<DHCPClient> rogueClients = FilterNonMatchingClients(allClients, settings.DetectionRegEx);

            //store list of detected clients and filter out old ones
            rogueClients = DHCPClientsStorage.Update(rogueClients);

            if (settings.SendEmail && rogueClients.Count > 0) Email.Send(rogueClients);

            return rogueClients;
        }

        private static List<DHCPClient> FilterNonMatchingClients(List<DHCPClient> allClients, string regex)
        {
            List<DHCPClient> result = new List<DHCPClient>();
            foreach (DHCPClient client in allClients)
            {
                try
                {
                    if (!Regex.IsMatch(client.hostname, regex)) result.Add(client);
                }
                catch (Exception oops)
                {
                    Debug.WriteLine("Could not check hostname " + client.hostname + ": " + oops.Message);
                }
            }

            return result;
        }
    }
}
