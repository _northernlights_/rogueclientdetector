﻿/*
 *  Rogue Client Detector - Copyright 2015 Clement Lorteau
 * 
 *  This file is part of Rogue Client Detector.
 *
 *  Rogue Client Detector is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Rogue Client Detector is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rogue Client Detector.  If not, see <http://www.gnu.org/licenses/>.
 */

using RCDLogic.DHCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace RCDLogic
{
    public static class Email
    {
        public static void Send(List<DHCPClient> clients)
        {
            Settings s = Settings.Load();

            string body = "The following rogue clients were detected on DHCP server " + s.DHCPServer + ":<br/><br/><ul>";
            foreach (DHCPClient c in clients)
            {
                body += "<li>" + c.hostname + ", IP: " + c.ip + ", MAC: " + c.mac + "</li>";
            }
            body += "</ul><br/>This is an automated email. Please don't reply.";

            MailMessage mail = new MailMessage(s.FromAddress, s.ToAddress);
            mail.Subject = s.Subject;
            mail.BodyEncoding = UTF8Encoding.UTF8;
            mail.IsBodyHtml = true;
            mail.Body = body;
            SmtpClient client = new SmtpClient();
            client.Host = s.SMTPServer;
            client.Port = (int)s.SMTPPort;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Send(mail);
        }
    }
}
