﻿/*
 *  Rogue Client Detector - Copyright 2015 Clement Lorteau
 * 
 *  This file is part of Rogue Client Detector.
 *
 *  Rogue Client Detector is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Rogue Client Detector is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rogue Client Detector.  If not, see <http://www.gnu.org/licenses/>.
 */

using RCDLogic;
using RCDLogic.DHCP;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace RCDService
{
    public class Service : ServiceBase
    {
        public static string ServiceNameString
        {
            get
            {
                string s = "Rogue Client Detector";
#if DEBUG
                s += " (Debug)";
#endif
                return s;
            }
        }
        public static int LaunchDetectionCommand { get { return 128; } }

        private Timer timer;
        private Settings settings;

        public Service()
        {
            this.ServiceName = ServiceNameString;
            this.AutoLog = true;
            this.CanHandleSessionChangeEvent = true;
            this.CanStop = true;
        }

        static void Main()
        {
            ServiceBase.Run(new Service());
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            timer = new Timer(2000); //initial call after 2 seconds
            timer.AutoReset = false;
            timer.Enabled = true;
            timer.Elapsed += timer_Elapsed;
            timer.Start();
        }

        protected override void OnCustomCommand(int command)
        {
            base.OnCustomCommand(command);

            if (command == LaunchDetectionCommand)
            {
                Do();
            }
        }

        public void Do()
        {
            String log = String.Empty;
            try
            {
                List<DHCPClient> clients = Detection.Run();
                if (clients.Count > 0)
                {
                    log += "The following rogue clients were detected:" + Environment.NewLine;
                    foreach (DHCPClient client in clients)
                    {
                        log += "Host name: " + client.hostname + ", IP: " + client.ip + ", MAC: " + client.mac + ", detection time: " + client.detectionTime + Environment.NewLine;
                    }
                    Log(log, false, true);
                }
                else
                {
                    log += "No new rogue clients detected";
                    Log(log);
                }
            }
            catch (Exception oops)
            {
                Log(oops + Environment.NewLine, true);
            }
        }

        private void Log(string message, bool error = false, bool warning = false)
        {
            if (error) this.EventLog.WriteEntry(message, EventLogEntryType.Error);
            else if (warning) this.EventLog.WriteEntry(message, EventLogEntryType.Warning);
            else this.EventLog.WriteEntry(message);

        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Do();
            settings = Settings.Load();
            timer.Interval = TimeSpan.FromMinutes(settings.CheckInterval).TotalMilliseconds;
            timer.Start();
        }

        protected override void OnStop()
        {
            base.OnStop();
            timer.Stop();
            timer.Dispose();
            timer = null;
        }
    }

}
