﻿/*
 *  Rogue Client Detector - Copyright 2015 Clement Lorteau
 * 
 *  This file is part of Rogue Client Detector.
 *
 *  Rogue Client Detector is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Rogue Client Detector is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rogue Client Detector.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RCDGUI
{
    public class Systray
    {
        public NotifyIcon NotifyIcon { get; internal set; }

        public event EventHandler ShowTestWindowRequested;
        public event EventHandler ShowConfigWindowRequested;
        public event EventHandler ExitRequested;
        public event EventHandler AboutRequested;

        public Systray()
        {
            NotifyIcon = new NotifyIcon();

            Stream appIconStream = System.Windows.Application.GetResourceStream(new Uri("pack://application:,,,/Resources/app.ico")).Stream;
            NotifyIcon.Icon = new Icon(appIconStream);
            appIconStream.Dispose();

            NotifyIcon.MouseUp += notifyIcon_MouseUp;

            #region context menu
            var notifyContextMenu = new System.Windows.Forms.ContextMenuStrip();

            var testWindowMenuItem = notifyContextMenu.Items.Add("Test");
            testWindowMenuItem.Click += ((object sender, EventArgs e) =>
                {
                    if (ShowTestWindowRequested != null)
                        ShowTestWindowRequested(this, null);
                });

            var configWindowMenuItem = notifyContextMenu.Items.Add("Config");
            configWindowMenuItem.Click += ((object sender, EventArgs e) =>
                {
                    if (ShowConfigWindowRequested != null)
                        ShowConfigWindowRequested(this, null);
                });

            notifyContextMenu.Items.Add(new ToolStripSeparator());

            var aboutMenuItem = notifyContextMenu.Items.Add("About");
            aboutMenuItem.Click += ((object sender, EventArgs e) =>
                {
                    if (AboutRequested != null)
                        AboutRequested(this, null);
                });

            var exitMenuItem = notifyContextMenu.Items.Add("Exit");
            exitMenuItem.Click += ((object sender, EventArgs e) =>
                {
                    if (ExitRequested != null)
                        ExitRequested(this, null);
                });
            #endregion

            NotifyIcon.ContextMenuStrip = notifyContextMenu;
            NotifyIcon.Visible = true;
        }

        void notifyIcon_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                MethodInfo mi = typeof(NotifyIcon).GetMethod("ShowContextMenu", BindingFlags.Instance | BindingFlags.NonPublic);
                mi.Invoke(NotifyIcon, null);
            }
        }

    }
}
