﻿/*
 *  Rogue Client Detector - Copyright 2015 Clement Lorteau
 * 
 *  This file is part of Rogue Client Detector.
 *
 *  Rogue Client Detector is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Rogue Client Detector is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rogue Client Detector.  If not, see <http://www.gnu.org/licenses/>.
 */

using RCDLogic;
using RCDLogic.DHCP;
using RCDService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace RCDGUI
{
    public partial class App : Application
    {
        private Systray Systray;
        private ConfigWindow ConfigWindow;
        private TestWindow TestWindow;
        private AboutWindow AboutWindow;
        private Settings Settings;

        public App()
        {
            Systray = new Systray();
            Settings = Settings.Load();
            this.ShutdownMode = System.Windows.ShutdownMode.OnExplicitShutdown;
        }

        public void StartUp(object sender, StartupEventArgs e)
        {
            Systray.AboutRequested += Systray_AboutRequested;
            Systray.ExitRequested += Systray_ExitRequested;
            Systray.ShowConfigWindowRequested += Systray_ShowConfigWindowRequested;
            Systray.ShowTestWindowRequested += Systray_ShowTestWindowRequested;

        }

        void TestWindow_TestStartRequested(object sender, EventArgs e)
        {
            try
            {
                ServiceController sc = new ServiceController(Service.ServiceNameString);
                ServiceControllerPermission scp = new ServiceControllerPermission(ServiceControllerPermissionAccess.Control, ".", Service.ServiceNameString);
                scp.Assert();
                sc.Refresh();
                sc.ExecuteCommand(Service.LaunchDetectionCommand);
                TestWindow.AddLogLine("Command sent to service");
            }
            catch (Exception oops)
            {
                TestWindow.AddLogLine("Error: " + oops.Message);
                if (oops.InnerException != null) TestWindow.AddLogLine("Error: " + oops.InnerException.Message);
                Debug.WriteLine(oops);
            }
        }

        void Systray_ShowTestWindowRequested(object sender, EventArgs e)
        {
            TestWindow = new TestWindow();
            TestWindow.TestStartRequested += TestWindow_TestStartRequested;
            TestWindow.Show();

            EventLog eventLog = new EventLog("Application", ".", Service.ServiceNameString);
            eventLog.EnableRaisingEvents = true;
            eventLog.EntryWritten += eventLog_EntryWritten;
        }

        void eventLog_EntryWritten(object sender, EntryWrittenEventArgs e)
        {
            App.Current.Dispatcher.Invoke(() =>
                {
                    TestWindow.AddLogLine(e.Entry.Message);
                });
        }

        void Systray_ShowConfigWindowRequested(object sender, EventArgs e)
        {
            ConfigWindow = new ConfigWindow();
            ConfigWindow.DataContext = Settings;
            ConfigWindow.Show();
        }

        void Systray_ExitRequested(object sender, EventArgs e)
        {
            ExitDialog dialog = new ExitDialog();
            dialog.ShowDialog();
            if (dialog.DialogResult == true)
            {
                if (dialog.StopService)
                {
                    StopService();
                }
                Systray.NotifyIcon.Visible = false;
                App.Current.Shutdown();
            }
        }

        private void StopService()
        {
            try
            {
                ServiceController sc = new ServiceController(Service.ServiceNameString);
                ServiceControllerPermission scp = new ServiceControllerPermission(ServiceControllerPermissionAccess.Control, ".", Service.ServiceNameString);
                scp.Assert();
                sc.Refresh();
                sc.Stop();
                TestWindow.AddLogLine("Stop command sent to service");
            }
            catch (Exception oops)
            {
                Debug.WriteLine(oops);
            }
        }

        void Systray_AboutRequested(object sender, EventArgs e)
        {
            AboutWindow = new AboutWindow();
            AboutWindow.Show();
        }
    }
}
