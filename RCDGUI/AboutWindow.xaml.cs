﻿/*
 *  Rogue Client Detector - Copyright 2015 Clement Lorteau
 * 
 *  This file is part of Rogue Client Detector.
 *
 *  Rogue Client Detector is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Rogue Client Detector is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rogue Client Detector.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RCDGUI
{
    public partial class AboutWindow : Window
    {
        public AboutWindow()
        {
            InitializeComponent();
            VersionTextBlock.Text = CurrentVersionString;
        }

        private void ProjectPageLink_MouseUp(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.lorteau.fr");
        }

        private void SourceCodeLink_MouseUp(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("https://bitbucket.org/_northernlights_/rogueclientdetector/src/");
        }

        private void Window_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private string CurrentVersionString
        {
            get
            {
                Version ver = Assembly.GetExecutingAssembly().GetName().Version;
                string version = string.Format("{0}.{1}.{2}", ver.Major, ver.Minor, ver.Build);
                return version;
            }
        }
    }
}
